
CREATE DATABASE  IF NOT EXISTS `deal_data`;

USE `deal_data`;
DROP TABLE IF EXISTS `deal`;
DROP TABLE IF EXISTS `counter_party`;
DROP TABLE IF EXISTS `instrument`;
CREATE TABLE `instrument` (
  `instrument_id` int NOT NULL AUTO_INCREMENT,
  `instrument_name` varchar(45) NOT NULL,
  PRIMARY KEY (`instrument_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO `instrument` 
VALUES (1, 'Astronomica'), 
(2, 'Borealis'), 
(3, 'Celestial'), 
(4, 'Deuteronic'),
(5, 'Eclipse'),
(6, 'Floral'), (7, 'Galactia'),
 (8, 'Heliosphere'), 
 (9, 'Interstella'), 
 (10, 'Jupiter'), (11, 'Koronis'), (12, 'Lunatic');



CREATE TABLE `counter_party` (
  `cp_id` int NOT NULL AUTO_INCREMENT,
  `cp_name` varchar(45) NOT NULL,
  PRIMARY KEY (`cp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- LOCK TABLES `counter_party` WRITE;
INSERT INTO `counter_party` 
VALUES 
(1, 'Lewis'), (2, 'Selvyn'), (3, 'Richard'), (4, 'Lina'), (5, 'John'), (6, 'Nidia');


CREATE TABLE `deal` (
  `deal_id` int NOT NULL AUTO_INCREMENT,
  `instrument_id` int NOT NULL,
  `cp_id` int NOT NULL,
  `price` int NOT NULL,
  `type` varchar(1) NOT NULL,
  `quantity` int NOT NULL,
  `time` datetime NOT NULL,
  PRIMARY KEY (`deal_id`),
  FOREIGN KEY(instrument_id) REFERENCES instrument(instrument_id),
  FOREIGN KEY(cp_id) REFERENCES counter_party(cp_id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `shadow`;
CREATE TABLE `shadow` (
	`user_name` varchar(15) NOT NULL,
    `password` varchar(50) NOT NULL,
    PRIMARY KEY(`user_name`)
);
INSERT INTO `shadow` 
VALUES ('root', 'root');


-- Select * from deal;
-- Delete FROM deal





